package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseClass {
	
	
	public WebDriver driver ;
	
	public void luanchURL (String browser, String url) {
		switch(browser) {
		
		case "chrome" : WebDriverManager.chromedriver().setup();
		                driver = new ChromeDriver();
		                break;
		
		case "firefox" : WebDriverManager.firefoxdriver().setup();
                         driver = new FirefoxDriver();
                         break;   
                         
         default:      WebDriverManager.chromedriver().setup();
                       driver = new ChromeDriver();
                       break;           
		}
		
		driver.get(url);
		driver.manage().window().maximize();
		
		
	}
	

}
