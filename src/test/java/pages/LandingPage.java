package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LandingPage {
	
	WebDriver driver;
	
	public LandingPage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	

	@FindBy (xpath = "//div[@class='category-cards']//div[1]//div[1]//div[2]//*[local-name()='svg']")
	WebElement elements;
	
	@FindBy (xpath = "//div[@class='element-list collapse show']//li[@id='item-0']")
	WebElement textBox ;
	
	
	public void elementClick() {
	JavascriptExecutor js = (JavascriptExecutor)driver ;
	js.executeScript("arguments[0].scrollIntoView();", elements);
	elements.click();
		
	}
	
	public void clickTextBox() {
		elementClick();
		textBox.click();
		
	}
}
