package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DemoQAElementsPage {
	
	WebDriver driver ;
	
	public DemoQAElementsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy (xpath = "//input[@id='userName']") 
	WebElement username;
	
	@FindBy (xpath = "//input[@id='userEmail']")
	WebElement userEmail;
	
	@FindBy (xpath = "//textarea[@id='currentAddress']")
	WebElement currentAddress;
	
	@FindBy (xpath = "//textarea[@id='permanentAddress']")
	WebElement permanentAddress;
	
	@FindBy (xpath = "//button[normalize-space()='Submit']")
	WebElement submit ;
	
	@FindBy (css = "div.border.col-md-12.col-sm-12")
	WebElement result ;
	
	
	public void enterName(String name) {
		username.sendKeys(name);
	}
	
	public void enterEmail(String email) {
		userEmail.sendKeys(email);
	}
	
	public void enterCurrentAddress(String currAddress) {
		
		currentAddress.sendKeys(currAddress);
	}
	
    public void enterPermannentAddress(String permAddress) {
		
    	permanentAddress.sendKeys(permAddress);
	}
    
    public void clickSubmit() {
    	
    	JavascriptExecutor js = (JavascriptExecutor)driver;
    	js.executeScript("arguments[0].scrollIntoView();", submit);
    	submit.click();
    }
    
    public String getResult() {
    	return result.getText();
    }
	

}
