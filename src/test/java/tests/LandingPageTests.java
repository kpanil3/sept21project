package tests;

import org.testng.Assert;

import org.testng.annotations.Test;

import base.BaseClass;
import listenerUtil.MyRetryAnalyzer;
import pages.LandingPage;

public class LandingPageTests extends BaseClass {
	
	LandingPage lp;
	
	
	@Test
	public void clickElements() {
		luanchURL("chrome", "https://demoqa.com/");
		lp = new LandingPage(driver);
		lp.elementClick();
		String current = driver.getCurrentUrl();
		Assert.assertEquals(current, "https://demoqa.com/elements");
		driver.close();
		
	}
	
	@Test
	
	public void testRetryLogic() {
		
		String test = "Anil";
		Assert.assertEquals(test, "Anil");
	}
	
	
	

}
