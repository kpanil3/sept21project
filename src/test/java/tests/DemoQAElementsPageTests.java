package tests;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


import base.BaseClass;
import pages.DemoQAElementsPage;
import pages.LandingPage;
import utils.UtilMethods;

public class DemoQAElementsPageTests extends BaseClass {
	
	LandingPage lp ;
	DemoQAElementsPage dq;
	UtilMethods util;
	@BeforeTest
	public void setup() {
		luanchURL("chrome", "https://demoqa.com/");
		lp = new LandingPage(driver);
		dq = new DemoQAElementsPage(driver);	
		util = new UtilMethods();
	
	}
	
	
	@Test
	public void testSubmit() throws IOException {
		
		lp.clickTextBox();
		dq.enterName("abcd");
		dq.enterEmail("a@b.com");
		dq.enterCurrentAddress("def");
		dq.enterPermannentAddress("xyz");
		dq.clickSubmit();
		util.getScreenShot(driver);
		String result = dq.getResult();
		System.out.println(result);
	}
	
	
@AfterTest
	
	public void close() {
		driver.close();
	}



}
