package listenerUtil;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class MyRetryAnalyzer implements IRetryAnalyzer {
	
	 int counter = 0;
	    int maxRetryCount = 10;
		public boolean retry(ITestResult result) {
			if (counter<maxRetryCount) {
				counter++;
				return true;
			}
			return false;
		}

}
