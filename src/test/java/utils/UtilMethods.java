package utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class UtilMethods {
	
	
	public void getScreenShot(WebDriver driver) throws IOException {
		
		TakesScreenshot ts = (TakesScreenshot)driver ;
		File srcFile = ts.getScreenshotAs(OutputType.FILE);
		File destFile = new File("c:\\Operations\\Example1.jpg") ;
		FileUtils.copyFile(srcFile, destFile);
	}

}
